export class CreateTypeTaskDto {
  readonly name: string;
  readonly value: number;
}
